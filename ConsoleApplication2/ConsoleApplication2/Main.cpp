#define CURL_STATICLIB
#include "iostream"
#include "stdio.h"
#include "string"

#include "curl/curl.h"

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
	((std::string*)userp)->append((char*)contents, size * nmemb);
	return size * nmemb;
}

int main(void)
{
	std::string readBuffer;
	CURL *hnd = curl_easy_init();

	curl_easy_setopt(hnd, CURLOPT_CUSTOMREQUEST, "POST");
	curl_easy_setopt(hnd, CURLOPT_URL, "http://development.sonicboom.co.id/API/v1/message/list_dev");

	struct curl_slist *headers = NULL;
	headers = curl_slist_append(headers, "postman-token: 5bfefd1e-11b9-b2b7-e0b5-039c6e618ceb");
	headers = curl_slist_append(headers, "cache-control: no-cache");
	curl_easy_setopt(hnd, CURLOPT_HTTPHEADER, headers);

	curl_easy_setopt(hnd, CURLOPT_POSTFIELDS, "{\"cid\":\"com.app.sonicboom\",\"bid\":\"1\",\"uid\":\"1\",\"otp\":\"959798\"}");
	curl_easy_setopt(hnd, CURLOPT_WRITEFUNCTION, WriteCallback);
	curl_easy_setopt(hnd, CURLOPT_WRITEDATA, &readBuffer);
	CURLcode ret = curl_easy_perform(hnd);
	std::cout << readBuffer << std::endl;
	/*std::string readBuffer;
	CURL *hnd = curl_easy_init();
	CURLcode res;

	curl_easy_setopt(hnd, CURLOPT_CUSTOMREQUEST, "GET");
	curl_easy_setopt(hnd, CURLOPT_URL, "http://my.sonicboom.co.id/API/v1/message/company_id/com.app.sonicboom");
	curl_easy_setopt(hnd, CURLOPT_WRITEFUNCTION, WriteCallback);
	curl_easy_setopt(hnd, CURLOPT_WRITEDATA, &readBuffer);
	res = curl_easy_perform(hnd);
	curl_easy_cleanup(hnd);

	std::cout << readBuffer << std::endl;*/
	/*it's okay to use code below or not*/
	/*struct curl_slist *headers = NULL;
	headers = curl_slist_append(headers, "postman-token: 96a37b3d-06bc-09d6-04f0-5d401fbf42ae");
	headers = curl_slist_append(headers, "cache-control: no-cache");
	curl_easy_setopt(hnd, CURLOPT_HTTPHEADER, headers);*/

	/*curl_easy_setopt(hnd, CURLOPT_VERBOSE, 1L);
	CURLcode ret = curl_easy_perform(hnd);*/
	
	std::cout << "\n\nPress any key for exit!";
	std::cin.get();
	return 0;
}